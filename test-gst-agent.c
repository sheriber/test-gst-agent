#include <gst/gst.h>
#include <gst/app/gstappsink.h>
#include <stdio.h>
#include <spice/stream-device.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <spice/enums.h>

typedef struct SpiceStreamFormatMessage
{
    StreamDevHeader hdr;
    StreamMsgFormat msg;
}SpiceStreamFormatMessage;

typedef struct SpiceStreamDataMessage
{
    StreamDevHeader hdr;
    StreamMsgData msg;
} SpiceStreamDataMessage;

static int fd;

static size_t
write_all(int fd, const void *buf, const size_t len)
{
    size_t written = 0;
    while (written < len) {
        int l = write(fd, (const char *) buf + written, len - written);
        if (l < 0 && errno == EINTR) {
            continue;
        }
        if (l < 0) {
             printf("write failed");
            return l;
        }
        written += l;
    }
    return written;
}

static int spice_stream_send_format(unsigned w, unsigned h, unsigned c)
{

    SpiceStreamFormatMessage msg;
    const size_t msgsize = sizeof(msg);
    const size_t hdrsize  = sizeof(msg.hdr);
    memset(&msg, 0, msgsize);
    msg.hdr.protocol_version = STREAM_DEVICE_PROTOCOL;
    msg.hdr.type = STREAM_TYPE_FORMAT;
    msg.hdr.size = msgsize - hdrsize; /* includes only the body? */
    msg.msg.width = w;
    msg.msg.height = h;
    msg.msg.codec = c;
    if (write_all(fd, &msg, msgsize) != msgsize) {
        printf("writeall failed\n");
    }
    return TRUE;
}

static int spice_stream_send_frame(const void *buf, const unsigned size)
{
    SpiceStreamDataMessage msg;
    const size_t msgsize = sizeof(msg);
    ssize_t n;

    memset(&msg, 0, msgsize);
    msg.hdr.protocol_version = STREAM_DEVICE_PROTOCOL;
    msg.hdr.type = STREAM_TYPE_DATA;
    msg.hdr.size = size; /* includes only the body? */
    if (write_all(fd, &msg, msgsize) != msgsize) {
        printf("writeall failed\n");
    }
    if (write_all(fd, buf, size) != size) {
        printf("writeall failed\n");
    }
    return TRUE;
}

static GstFlowReturn new_sample(GstAppSink *gstappsink, gpointer capture)
{

    static int first = TRUE;
    GstSample *sample = gst_app_sink_pull_sample(gstappsink);
    if (sample) {
        GstBuffer *buffer = gst_sample_get_buffer(sample);
        GstMapInfo map;
        gst_buffer_map (buffer, &map, GST_MAP_READ);

        if (first) {
            int x=0,y=0;
            g_object_get(capture,
                         "endx", &x,
                         "endy", &y,
                         NULL);//check values validity?
            spice_stream_send_format(x, y, SPICE_VIDEO_CODEC_TYPE_H264);
            first = FALSE;
            sleep(1);
        }
        spice_stream_send_frame(map.data,  map.size);
        gst_buffer_unmap(buffer, &map);
        gst_sample_unref(sample);
    }
    return GST_FLOW_OK;
}


int main(int argc, char *argv[])
{
    GstElement *pipeline, *capture, *encoder, *convert, *sink;
    GstBus *bus;
    GstMessage *msg;
    GstCaps *caps;
    GstAppSinkCallbacks appsink_cbs = { NULL };

    /* Initialize GStreamer */
    gst_init (&argc, &argv);

    pipeline = gst_pipeline_new ("pipeline");

    capture = gst_element_factory_make ("ximagesrc", "capture");
    convert = gst_element_factory_make ("videoconvert", "convert");
    encoder = gst_element_factory_make ("x264enc", "encoder");
    sink = gst_element_factory_make ("appsink", "sink");
    if (!capture || !convert || !encoder || !sink) {
        g_printerr ("Not all elements could be created.\n");
        return -1;
    }

    caps = gst_caps_from_string("video/x-h264,stream-format=(string)byte-stream");
    g_object_set(sink,
                 "caps", caps,
                 "sync", FALSE,
                 "drop", FALSE,
                 NULL);
    gst_caps_unref(caps);

    g_object_set(capture,
                 "use-damage", 0,
                 NULL);

    g_object_set(encoder,
                 "bframes", 0,
                 "tune", "zerolatency",//stillimage,fastdecode,zerolatency
                 "speed-preset", 1, //1-ultrafast,6-med, 9-veryslow
                 NULL);

    gst_bin_add_many (GST_BIN (pipeline), capture, convert, encoder, sink, NULL);

    caps = gst_caps_from_string("video/x-raw,format='I420'");
    if (gst_element_link(capture, convert) &&
        gst_element_link_filtered (convert, encoder, caps) &&
        gst_element_link(encoder, sink))
        printf("link ok\n");
    else
        printf("link fail\n");

    gst_caps_unref(caps);

    appsink_cbs.new_sample = new_sample;
    gst_app_sink_set_callbacks(GST_APP_SINK(sink), &appsink_cbs, capture, NULL);

    fd = open("/dev/virtio-ports/com.redhat.stream.0", O_RDWR);
    if (fd < 0) {
        printf("fail to open device\n");
        close(fd);
        return 2;
    }

    /* Start playing */
    gst_element_set_state (pipeline, GST_STATE_PLAYING);

    /* Wait until error or EOS */
    bus = gst_element_get_bus (pipeline);
    msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

    /* Free resources */
    if (msg != NULL)
        gst_message_unref (msg);
    gst_object_unref (bus);
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    close(fd);
    return 0;
}
